$(document).ready(function () {
    let discountModal = $('#discountModal');

    eventSendForm('#js-discount-get', 'DiscountGet');
    eventSendForm('#js-discount-check', 'DiscountCheck');

    function eventSendForm(form, action) {
        $(form).submit(function (event) {
            let formData = $(this).serialize(),
                formButton = $(this).find('.btn'),
                loader = $(this).find('.js-loader');

            event.preventDefault();
            formButton.prop('disabled', true);
            loader.show();

            BX.ready(function () {
                BX.ajax.runComponentAction('test_officemag:discount', action, {
                    mode: 'class',
                    data: formData
                }).then(function (response) {
                    let messageTitle = response.data['title'],
                        messageBody = response.data['body'];

                    formButton.prop('disabled', false);
                    loader.hide();
                    discountModal.find('.modal-title').html(messageTitle);
                    discountModal.find('.modal-body').html(messageBody);
                    discountModal.modal('show')
                });
            });
        });
    }
});