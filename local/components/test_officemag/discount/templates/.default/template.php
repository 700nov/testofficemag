<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="row">
	<div class="col-12 mb-3">
		<form id="js-discount-get">
			<div class="input-group">
				<button class="btn btn-primary btn-md col-12 col-md-6 col-lg-4" type="submit" value="1" name="discountGet">
					<span class="js-loader spinner-border spinner-border-sm" style="display:none;" role="status" aria-hidden="true"></span>
					Получить скидку
				</button>
			</div>
		</form>
	</div>
	<div class="col-12 mb-5">
		<form id="js-discount-check">
			<div class="input-group">
				<button class="btn btn-info btn-md col-12 col-md-6 col-lg-4" type="submit" value="1" name="discountCheck">
					<span class="js-loader spinner-border spinner-border-sm" style="display:none;" role="status" aria-hidden="true"></span>
					Проверить скидку
				</button>
				<input type="text" maxlength="20" placeholder="Пример: SL-B77IH-8YONA6Q" name="discountCheckInput" class="form-control" required>
			</div>
		</form>
	</div>
</div>
<!-- Модальное окно -->
<div class="modal fade bd-example-modal-sm" id="discountModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="mySmallModalLabel">Скидка</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				...
			</div>
		</div>
	</div>
</div>