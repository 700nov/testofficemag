<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Sale\Internals\DiscountTable;
use Bitrix\Sale\Internals\DiscountCouponTable;
use Bitrix\Main\Application;
use Bitrix\Main\Engine\CurrentUser;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Type\DateTime;

Bitrix\Main\Loader::includeModule("sale");

// Класс компонента индивидуального купона скидки для пользователя
class CDiscount extends CBitrixComponent implements Controllerable
{

    // Конструктор с подключением шаблона компонента
    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }

    // Конфигуратор с префильтрами аутентификации пользователя, http-метода (только POST), Csrf-подписи
    public function configureActions()
    {
        return [
            'DiscountGet' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ],
            'DiscountCheck' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_POST)
                    ),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ]
        ];
    }

    // Получаем скидку
    public static function DiscountGetAction()
    {
        $user = CurrentUser::get(); // Пользователь
        $cache = Cache::createInstance(); // Кэш
        $discountMessage = [];

        $userId = $user->getId();

        // Если кэш на один час есть
        if ($cache->initCache(60 * 60, "test_" . $userId, 'discounts/users/' . $userId)) {
            $arrDiscount = $cache->getVars(); // достаем переменные из кэша
        } elseif ($cache->startDataCache()) { // Если кэша на час нет (закончился)

            $discountValue = rand(1, 50); // Генерируем размер случайной скидки от 1 до 50 процентов

            // Ищем id скидки с подходящим процентом ($discountValue)
            $arrDiscount = DiscountTable::getList([
                'select'  => ["ID"],
                'filter'  => ["XML_ID" => "discount_" . $discountValue . "_percent"]
            ])->fetch();

            if (count($arrDiscount) > 0) {
                $discountID = $arrDiscount['ID']; // ID подходящей скидки, если нашли
            }

            // Если id скидки не найден
            if (!$discountID) {
                $Conditions["CLASS_ID"] = "CondGroup";
                $Conditions["DATA"]["All"] = "AND";
                $Conditions["DATA"]["True"] = "True";
                $Conditions["CHILDREN"] = [];

                $Actions["CLASS_ID"] = "CondGroup";
                $Actions["DATA"]["All"] = "AND";
                $Actions["CHILDREN"][0]["CLASS_ID"] = "ActSaleBsktGrp";
                $Actions["CHILDREN"][0]["DATA"]["Type"] = "Discount";
                $Actions["CHILDREN"][0]["DATA"]["Value"] = $discountValue;
                $Actions["CHILDREN"][0]["DATA"]["Unit"] = "Perc";
                $Actions["CHILDREN"][0]["DATA"]["Max"] = 0;
                $Actions["CHILDREN"][0]["DATA"]["All"] = "AND";
                $Actions["CHILDREN"][0]["DATA"]["True"] = "True";
                $Actions["CHILDREN"][0]["CHILDREN"] = [];

                //Массив для создания правила
                $arFields = array(
                    "LID" => "s1",
                    "NAME" => "Скидка " . $discountValue . "%",
                    "ACTIVE_FROM" => '',
                    "ACTIVE_TO" => '',
                    "ACTIVE" => "Y",
                    "SORT" => $discountValue,
                    "PRIORITY" => 1,
                    "LAST_DISCOUNT" => "Y",
                    "LAST_LEVEL_DISCOUNT" => "N",
                    "XML_ID" => "discount_" . $discountValue . "_percent",
                    'CONDITIONS' => $Conditions,
                    'ACTIONS' => $Actions,
                    "USER_GROUPS" => [6], // Зарегистрированные пользователи
                );

                //Создаем правило корзины с нужной скидкой
                $discountID = CSaleDiscount::Add($arFields);
            }

            // Создаем купон
            $couponCode = DiscountCouponTable::generateCoupon(true);
            $dateTime = new DateTime;
            $addDb = DiscountCouponTable::add(array(
                'DISCOUNT_ID' => $discountID,
                'ACTIVE_TO' => $dateTime->add('+3 hours'), // Период активности задаем в три часа
                'COUPON' => $couponCode,
                'TYPE' => DiscountCouponTable::TYPE_ONE_ORDER,
                'MAX_USE' => 1,
                'USER_ID' => $userId,
            ));

            // Готовим данные для кэша
            $arrDiscount = [
                "code" => $couponCode,
                "value" => $discountValue
            ];

            // Записываем в кэш
            $cache->endDataCache($arrDiscount);
        }

        // формируем массив итогового сообщения
        $discountMessage['title'] = 'Ваша скидка: ' . $arrDiscount['value'] . '%';
        $discountMessage['body'] = 'Код вашего купона на скидку:<br><b>' . $arrDiscount['code'] . '</b>';

        return $discountMessage;
    }

    // Проверяем скидку
    public static function DiscountCheckAction($discountCheckInput)
    {
        $user = CurrentUser::get(); // Пользователь
        $discountMessage = [];

        $userId = $user->getId();

        // Если discountCheckInput передан
        if ($discountCheckInput) {
            $discountCheckInput = trim($discountCheckInput); // Чистим от пробелов
            $discountCheckInput = htmlspecialcharsbx($discountCheckInput); // Приводим в безопасный вид

            $dateTime = new DateTime;

            // Ищем id купона для текущего пользователя и с действующим периодом активности
            $result = DiscountCouponTable::getList([
                'select'  => ["DISCOUNT_ID"],
                'filter'  => ["COUPON" => $discountCheckInput, "USER_ID" => $userId, ">ACTIVE_TO" => $dateTime->toString()]
            ]);
            $arrDiscountCoupon = $result->Fetch();

            if (count($arrDiscountCoupon) > 0) {
                $discountCouponID = $arrDiscountCoupon["DISCOUNT_ID"];
            }

            // Если id купона найден
            if ($discountCouponID) {
                // Ищем процент скидки
                $result = DiscountTable::getList([
                    'select'  => ["SHORT_DESCRIPTION_STRUCTURE"],
                    'filter'  => ["ID" => $discountCouponID]
                ]);

                $arrDiscount = $result->Fetch();

                if (count($arrDiscount) > 0) {
                    $discountID = $arrDiscount["SHORT_DESCRIPTION_STRUCTURE"]["VALUE"];
                }
            }

            $discountMessage['title'] = 'Ваша скидка';
            if ($discountID) { // Если скидка для пользователя найдена и не является "просроченной"
                $discountMessage['body'] = '<b>' . $discountID . '%</b>';
            } else { // Иначе
                $discountMessage['body'] = 'Скидка недоступна';
            }
        } else { // Если discountCheckInput отсутствует
            $discountMessage['title'] = 'Отсутствует номер купона';
            $discountMessage['body'] = 'Введите номер купона. Например:<br> SL-B77IH-8YONA6Q';
        }

        // формируем массив итогового сообщения
        return $discountMessage;
    }
}
